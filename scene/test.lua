local scene = gamestate.new()
local Stage = require "cls/stage"
local static = require "cls/spineActor"
local player = require "cls/player"
local npc = require "cls/role"
local girls = require "cls/girl"
local img = require "cls/imageActor"

local joystick = require "cls/joystick"()
local Button = require "cls/button"
local buttonA = Button(1100,500,100,80,"jump")
local buttonB = Button(1000,600,100,80,"action")

local debugfont  = love.graphics.newFont(12)

function scene:init()
	self.stage = Stage("test")
	local stage= self.stage
	
	local inner = static(stage,"beijing",0,0,0)
	local mid = static(stage,"zhongjing",0,0,10)
	local grass = static(stage,"caoping",0,0,11)
	local outer1 = static(stage,"qianjing1",0,0,1100)
	local outer2 = static(stage,"qianjing2",0,0,1200)
	local outer3 = static(stage,"qianjing3",0,0,1300)
	
	hero = player(stage,"player", 600,0,200)
	self.stage:setCameraFocus(hero)
	
	local n1 = npc(stage,"player", 200,0,200)
    local n1 = npc(stage,"player", 300,0,300)
    local n1 = npc(stage,"player", 400,0,400)
    local n1 = npc(stage,"player", 500,0,450)
    local n1 = npc(stage,"player", 700,0,500)

    local girl = girls(stage,"girls1",900,0,200)

	buttonA.onClick=function(b)
		hero:jump()
	end
	buttonB.onClick=function(b,holdtime)
		if not hero.canAttack then return end
		hero:attackPress(holdtime)
	end

	buttonB.onDown = function(b)
		if not hero.canAttack then return end
		if hero.attackHolding then
			hero:attackHold()
		else
			hero:attackDown()
		end		
	end
end 

function scene:enter()
	
end

function scene:leave()
	
end


function scene:draw()
	self.stage:draw()
	joystick:draw()
	buttonA:draw()
	buttonB:draw()
	--[[
	love.graphics.setColor(255, 0, 0, 255)
	love.graphics.setFont(debugfont)
	love.graphics.print("Mega State: "..hero.state.current.name, 100,100)
	love.graphics.print("isAttacking: "..tostring(hero.attacking), 100,130)
	love.graphics.print("attack key: "..tostring(hero.attackKey), 100,160)
	love.graphics.print("attack can: "..tostring(hero.canAttack), 100,190)]]
end

function scene:update(dt)
	delay:update(dt)
	joystick:update()
	hero:moveByStick(joystick.vx,joystick.vy)
	buttonA:update()
	buttonB:update()
	self.stage:update(dt)
	love.window.setTitle(love.timer.getFPS())
end 



function scene:keyDown()

end


function scene:keypressed(key)
	hero:keypress(key)
end


return scene