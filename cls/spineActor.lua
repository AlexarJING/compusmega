local Debug = true --调试模式
local path = "res/spine"

----------------------------------------------libs----------------------------
local spineActor= class("spineActor")

-------------------------------------------func----------------------------

function spineActor:init(stage,name,x,y,z,scale)
	self:initProperties(x,y,z,scale,stage)
	self:initAnim(name,x,y)	
end


function spineActor:initAnim(name,x,y)
	--(path, name, animation, skin, scale, x, y)
	self.skeleton, self.animState= spine.loadSkeleton(path,name,_,_,self.scale,x,y)
end

function spineActor:initProperties(x,y,z,scale,stage)
	self.x=x
	self.y=y
	self.z= z
	self.scale = scale or 1
	self.r= 0
	self.sx=1
	self.sy =1	
	self.stage= stage
	stage:addActor(self)
end


function spineActor:playAnim(name,loop,add,delay,speed)
	if add then
		self.currentAnim.loop = false
		self.currentAnim = self.animState:addAnimationByName (0, name, loop, delay or 0)
	else
		self.currentAnim = self.animState:setAnimationByName (0, name, loop)
	end
	self.currentAnim.timeScale=speed or 1
end

function spineActor:updateSkeleton(dt)
	self.animState:update(dt)
	self.animState:apply(self.skeleton)
	self.skeleton:updateWorldTransform()
end

function spineActor:update(dt)	
	self:updateSkeleton(dt)
end


function spineActor:draw()	
	love.graphics.setColor(255, 255, 255, 255)
	spine.renderer:draw(self.skeleton)
end

return spineActor