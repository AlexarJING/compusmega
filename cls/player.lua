local role = require "cls/role"
local player= class("player",role)
local actionKey = "rctrl"


function player:init(stage,x,y,z,name,subname)
	player.super.init(self,stage,x,y,z,name,subname)
	self.animState:setAnimationByName (0, "idle1", true)
end

function player:keyMove()
	if self.stickWorking then return end
	if not self.canMove then return end

	if love.keyboard.isDown("a") then
		self.ax = -self.speed 
	elseif love.keyboard.isDown("d") then
		self.ax = self.speed 
	else
		self.ax = 0
	end
	if  love.keyboard.isDown("w") then
		self.az =  -self.speed
	elseif  love.keyboard.isDown("s") then
		self.az =  self.speed
	else
		self.az = 0
	end

	if  love.keyboard.isDown("lshift") then
		self.isRunning = true
	else
		self.isRunning = false
	end


end

function player:keyAttack()
	if self.canAttack then
		if love.keyboard.isDown(actionKey) then
			if self.attackHolding then
				self:attackHold()
			else
				self:attackDown()
			end
		elseif self.attackHolding then
			self:attackPress()
		end
	end
end


function player:attackDown()
	self.canMove = false
	self.attackHolding = true
	self.attackDownTime = love.timer.getTime()
	self.attackPower = 0
end

function player:attackHold(dx,dy)
	local dt = love.timer.getDelta()
	self.attackPower = self.attackPower + dt*100
	local key
	local isDown = love.keyboard.isDown
	if isDown("w") then
		key = "up"
	elseif isDown("s") then
		key = "down"
	elseif isDown("a") then
		if self.facingRight then
			key = "back"
		else
			key = "front"
		end
	elseif isDown("d") then
		if self.facingRight then
			key = "front"
		else
			key = "back"
		end
	else
		key = "none"
	end

	self.attackKey = key
end

function player:attackPress()
	self.attackHoldTime = love.timer.getTime() - self.attackDownTime
	self.attackHolding = false
	self:attack()
end


function player:keypress(key)
	if key=="space" then self:jump() end
end

function player:attack()
	self.comboLevel = self.comboLevel + 1
	if self.comboLevel > self.maxComboLevel then
		self.comboLevel = self.maxComboLevel
	end
	self.comboTimer = 1.5
	self.attacking = true
end

function player:attackComboCheck(dt)
	self.comboTimer = self.comboTimer - dt
	if self.comboTimer<0 then
		self.comboLevel = 0
	end 
end

function player:moveByStick(sx,sy)
	self.stickWorking=false
	if not self.canMove then return end
	if sx==sy and sy==0 then return end
	self.stickWorking = true
	if math.abs(sx)>0.8 or math.abs(sy)>0.8 then self.isRunning=true end
	if math.abs(sx)<0.3 then sx=0 end
	if math.abs(sy)<0.3 then sy=0 end
	
	self.ax = self.speed * math.sign(sx)
	self.az = -self.speed * math.sign(sy)
end

function player:update(dt)
	self:keyMove()
	self:keyAttack()
	self:attackComboCheck(dt)
	player.super.update(self,dt)
end

function player:draw()
	player.super.draw(self)
	--[[
	love.graphics.setColor(255, 0, 0, 255)
	love.graphics.rectangle("line", self.x - self.w/2, self.y +self.z/4 -self.h - 100, 150, 20)
	love.graphics.setColor(255, 0, 0, 155)
	local w = self.comboTimer < 0 and 0 or 150*self.comboTimer/1.5
	love.graphics.rectangle("fill", self.x - self.w/2, self.y +self.z/4 -self.h - 100, w, 20)
	love.graphics.printf(self.comboLevel, self.x - self.w/2, self.y +self.z/4 -self.h - 100, 150,"center")]]
end

return player