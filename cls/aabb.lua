local aabb = class("aabb")


function aabb:init(parent)
	self.parent = parent
	self.world = self.parent.stage.world
	self.parts = {}
end

function aabb:addPart(tag,offx,offy,w,h,targets)
	local parent = self.parent
	local x = parent.x + offx
	local y = parent.y + offy + parent.z/4
	local userdata = {
		tag = tag,
		offx = offx,
		offy = offy,
		w = w,
		h = h,
		parent = parent,
		targets = targets or {}
	}
	part = self.world:rectangle(x, y, w, h)
	part.userdata = userdata
	part.onCollision = function() return end
	table.insert(self.parts,part)
	return part
end

function aabb:apply()
	local parent = self.parent
	for i,part in ipairs(self.parts) do
		local userdata = part.userdata
		local x = parent.x + userdata.offx
		local y = parent.y + userdata.offy + parent.z/4
		part:moveTo(x,y)
	end
end

function aabb:test()
	local me = self.parent
	for i,part in ipairs(self.parts) do
		local mydata =part.userdata
		for other, delta in pairs(self.world:collisions(part)) do
	       	local otherdata = other.userdata
	       	local you = otherdata.parent
	       	if me~=you and table.getIndex(mydata.targets, otherdata.tag) then --不跟自己碰撞,只跟target碰撞
	       		if (me.z>=you.z and me.z - you.z < me.l*4) or  --不跟z不相交的碰撞
	       			(you.z> me.z and you.z - me.z < you.l*4) then
	  				local result , rate= part:onCollision(other,delta,otherdata.tag) , 1
	  				if result == "pass" then
	  					--nothing
	  				elseif result == "slide" then
	  					if me.dy>=0 then --下降
	  						if you.y - me.y<= you.h then ---有碰触
	  							if you.y - (me.y-me.dy) >=  you.h then --如果是从上面下来的
	  								me.y = me.y - me.dy
	  								me.dy = 0
	  							end
	  						end
		  				end
	  				elseif result == "slideVert" then
	  					if math.sign(me.dx) ~= math.sign(delta.x) then
	  						me.x = me.x - me.dx
	  						me.dx = 0
	  					end

	  					if math.sign(you.z - me.z) == math.sign(me.dz) then
	  						me.z = me.z - me.dz
	  						me.dz = 0
	  					end
	  				elseif result == "bounce" then
	  					me.x = me.x - me.dx
	  					me.dx = -me.dx*rate
	  					me.z = me.z - me.dz
	  					me.dy = -me.dz*rate
	  				elseif result == "touch" then
	  					if math.sign(me.dx) ~= math.sign(delta.x) then
	  						me.x = me.x - me.dx
	  						me.dx = 0
	  					end

	  					if math.sign(me.dy) ~= math.sign(delta.y) then
	  						me.y = me.y - me.dy
	  						me.dy = 0
	  					end

	  					if math.sign(you.z - me.z) == math.sign(me.dz) then
	  						me.z = me.z - me.dz
	  						me.dz = 0
	  					end
	  				else
	  					-- nothing
	  				end
	  				self:apply()
	  			end
	       	end
	    end
	end
end

function aabb:draw()
	for i,part in ipairs(self.parts) do
		part:draw()
	end
end

return aabb