--local Debug = true --调试模式
local RoleSize = 0.4 --人物比例尺
local RoleHeight = 140 --人物高度
local RoleWidth = 90 --人物宽度
local RoleLength = 20 --人物厚度
local RoleSpeed = 1 --人物加速度

local AttackZoneWidth = 50 --攻击区域宽度
local AttackComboCD =0.5 --攻击连击冷却
local DamagedCD =0.3 --连续受击间隔

local SCENE_WIDTH=2000 --场景宽度
local SCENE_HEIGHT=2000 --场景高度
local SCENE_LENTH=500 --场景深度


----------------------------------------------libs----------------------------
local SpineActor = require "cls/spineActor"
local role= class("role",SpineActor)
local stateSystem = require "lib/roleStateSystem"
local states = require "lib/standardRoleState"
local AABB = require "cls/aabb"
-------------------------------------------func----------------------------


function role:init(stage,name,x,y,z,scale)
	self:initProperties(x,y,z,scale,stage)
	self:initAnim(name,x,y)
	self:initState()
	self:initAABB()
	self.static = false
end

function role:initAnim(name,x,y)
	local path = "res/spine"
	local name = name
	self.skeleton, self.animState= spine.loadSkeleton(path,name,_,_,self.scale,x,y)
end

function role:initProperties(x,y,z,scale,stage)
	self.x=x
	self.y=y
	self.z= z
	self.r= 0
	self.sx=1
	self.sy =1	
	self.scale = scale  or RoleSize
	self.stage= stage
	stage:addActor(self)


	self.w=RoleWidth
	self.h=RoleHeight
	self.l=RoleLength

	self.debug=Debug
	self.comboCD = AttackComboCD

	self.speed=RoleSpeed
	self.dx=0
    self.dy=0
    self.dz=0
	self.ax=0
	self.az=0
	self.ay=0.8
	self.damping = 0.8

	self.isRunning=false
	self.onGround=true
	self.facingRight = true
	
	self.comboTimer = self.comboCD
	self.comboLevel = 0
	self.maxComboLevel = 3

	self.canMove = true
	self.canAttack = true
	self.canJump = true
end

function role:initState()
	self.stateSystem = stateSystem 
	self.state = stateSystem.init(self)
	for name,action in pairs(states) do
		self.state:reg(action,name=="idle")
	end
	self.state:switch(nil , self.state.stack["idle"])
end

function role:initAABB()
	self.aabb = AABB(self)
	local aabb = self.aabb
	--(tag,offx,offy,w,h,l,targets)

	local h = RoleHeight --人物高度
	local w = RoleWidth--人物宽度
	local l = RoleLength

	local x = w/2
	local y = -h/2

	local body = aabb:addPart("body", x, y , w , h ,{"body"})
	body.onCollision = function(body,other,delta,tag)
		if self.standingOnObject then return end
		return "slideVert"
	end
	self.body = body

	local head = aabb:addPart("head", x , y-h/2-l/2, w, l,{"head"})
	head.onCollision = function(head,other,delta,tag)
		return "slideVert"
	end
	self.head = head

	local foot = aabb:addPart("foot",x, y+h/2-l/2, w, l ,{"head","foot"})
	foot.onCollision = function(foot,other,delta,tag)		
		if tag == "foot" then
			return "slideVert"
		else
			self.standingOnObject = true
			return "slide"
		end
	end
	self.foot = foot

	local left = aabb:addPart("left",x-w/2-AttackZoneWidth/2,y,AttackZoneWidth,h/2,{"body"})
	left.onCollision = function(left,other,delta,tag)
		if self.leftEnabled then
			self:damageTo(other.userdata.parent)
			--print("hit ".. tostring(other.userdata.parent))
		end
	end
	self.left = left

	local right = aabb:addPart("right",x+w/2+AttackZoneWidth/2,y,AttackZoneWidth,h/2,{"body"})
	right.onCollision = function(right,other,delta,tag)
		if self.rightEnabled then
			self:damageTo(other.userdata.parent)
			--print("hit ".. tostring(other.userdata.parent))
		end
	end
	self.right = right
end


function role:playAnim(name,loop,add,delay,speed)
	if add then
		if self.currentAnim then self.currentAnim.loop = false end
		self.currentAnim = self.animState:setNextAnimationByName (0, name, loop, delay or 0)
	else
		self.currentAnim = self.animState:setAnimationByName (0, name, loop)
	end
	self.currentAnim.timeScale=speed or 1
end

function role:stopCurrentAnim()
	self.currentAnim.loop = false
	self.currentAnim.endTime = 0
end


function role:borderTest()
	if self.x < 0  then self.x = 0 end
	if self.x > 2100 then self.x = 2100 end
	if self.z < 0 then self.z = 0 end
	if self.z > 1000 then self.z = 1000 end
	if self.y > 0 then self.y = 0 ; self.dy = 0 end
end


local attackEffect = {
	leftPunch = {damage = 7, push = 1, down = 0, up = 0},
	rightPunch = {damage = 10, push = 2, down = 0, up = 0},
	doubleLeftPunch = {damage = 10, push = 1, down = 0, up = 0},
	leftHook = {damage = 15, push = 1, down = 0, up = 10},
	rightRush = {damage = 13, push = 5, down = 0, up = 10},
	roundKick = {damage = 15, push = 3, down = 0, up = 10},
	jumpKick = {damage = 12, push = 3, down = 0, up = 10},
	rushKick = {damage = 12, push = 3, down = 0, up = 10},
	tackleKick = {damage = 12, push = 3, down = 0, up = 10},
	rushRoll = {damage = 12, push = 3, down = 0, up = 10},
}

function role:damageTo(other)
	local attackName = self.state.current.name
	local backHit 
	if (other.facingRight and self.x < other.x) or 
		(not other.facingRight and self.x > other.x) then 
		backHit = true
	end
	other:getHit(self,attackName,backHit)
end


function role:getHit(attacker,attackName,backHit)
	local effect = attackEffect[attackName]
	local heavy = effect.damage >10
	local push = effect.push
	local up = effect.up

	if attacker.facingRight then
		self.dx = push
		self.dy = up
	else
		self.dx = -push
		self.dy = up
	end



	if heavy then
		if not backHit then
			self.state:switch(nil , self.state.stack["behitFrontHeavy"])
		else
			self.state:switch(nil , self.state.stack["behitBackHeavy"])
		end
	else
		if not backHit then
			self.state:switch(nil , self.state.stack["behitFrontLight"])
		else
			self.state:switch(nil , self.state.stack["behitBackLight"])
		end
	end
	
end

function role:updateSkeleton(dt)
	self.skeleton.flipX = not self.facingRight
	self.skeleton.x=self.x+self.w/2
	self.skeleton.y=self.y+self.z/4
	self.animState:update(dt)
	self.animState:apply(self.skeleton)
	self.skeleton:updateWorldTransform()
end

function role:translate()
	local multiply = self.isRunning and 2 or 1
	self.dx = self.dx + self.ax*multiply
	self.dz = self.dz + self.az*multiply
	self.dy = self.dy + self.ay

	self.dx=self.dx*self.damping
	self.dz=self.dz*self.damping

	if math.abs(self.dx)<0.1 then self.dx=0 end
	if math.abs(self.dz)<0.1 then self.dz=0 end
		
	self.x = self.x + self.dx
	self.z = self.z + self.dz
	self.y = self.y + self.dy
end


function role:moveTo(x,y,z)
	self.x=x
	self.y=y
	self.z=z
end

function role:jump()
	if not self.canJump then return end
	if self.isRunning then
		self.dy = -18
	else
		self.dy = -12
	end
	
	self.onGround=false
	self.standingOnObject=false
end



function role:attackTiming(name,ending)
	if name=="hittime" then
		self:enableAttackZone(true)
	elseif name=="chuangetime" and not ending then
		--self.canAttack=true
	elseif name=="hittimeend" then
		self:enableAttackZone(false)
	end
end

function role:enableAttackZone(enable)
	if self.facingRight then
		self.leftEnabled = false
		self.rightEnabled = enable
	else
		self.leftEnabled = enable
		self.rightEnabled = false
	end
end

function role:updateMoveState()

	if self.y == 0 then
		self.onGround = true
	else
		self.onGround = false
	end

	self.canJump = self.onGround or self.standingOnObject

	if self.dx==0 and self.dy==0 and self.dz==0 then
		self.isMoving=false
	else
		self.isMoving=true
	end
	
	if math.sign(self.ax) ~= math.sign(self.dx) and self.dx ~= 0 then
		self.turning = true
	else
		self.turning = false
	end

	if not self.turning then
		if self.dx>0 then
			self.facingRight = true
		elseif self.dx<0 then 
			self.facingRight = false 
		end
	end


	self.ax=0
	self.az=0
end


function role:update(dt)

	self:translate()		
	self:borderTest()
	self.aabb:apply()
	self.aabb:test()
	self.aabb:apply()
	self:updateMoveState()
	self.state:update()
	self:updateSkeleton(dt)
end


function role:draw()	
	role.super.draw(self)
	if Debug then
		love.graphics.setColor(255, 0, 0, 255)
		self.aabb:draw()
	end
end

return role