local states={}

states.idle={
	name="idle",
	relative={"turn","walk","run","jump",
	"leftPunch",
	"doubleLeftPunch","rightPunch",
	"rightRush","roundKick","leftHook",
	},
	onEnter = function(role) 
		role:playAnim("idle1",true,true)
 	end,
	condition = function(role) return not role.isMoving end,
	onExit = function(role)
		role:stopCurrentAnim()
	end
}

states.walk={
	name="walk",
	relative={"run","jump","leftPunch",
	"doubleLeftPunch","rightPunch",
	"rightRush","roundKick","leftHook",},
	onEnter = function(role) 
		role:playAnim("move",true,true)
 	end,
	condition = function(role) 
		return role.isMoving and not role.isRunning
	end,
	onExit = function(role)
		role:stopCurrentAnim()
	end
}

states.run={
	name="run",
	relative={"tackleKick","rushRoll","jerk","walk","jump"},
	onEnter = function(role) 
		role:playAnim("run",true,true)
 	end,
	condition = function(role) 
		return role.isRunning and role.isMoving
	end,
	onExit = function(role)
		role:stopCurrentAnim()
	end
}

states.turn={
	name="turn",
	lasting = true,
	relative={"walk","idle","run"},
	onEnter = function(role)
		role:playAnim("idleturn")
		role.currentAnim.onEnd = function()
			role.turning = false
		end
 	end,
	condition = function(role)
		return role.turning and not role.isRunning
	end,
	onExit = function(role)
		role.turning = false
	end
}


states.jerk={
	name="jerk",
	lasting = true,
	relative={"walk","idle","run"},
	onEnter = function(role)
		role:playAnim("runjerk")
		role.currentAnim.onEnd = function()
			role.turning = false
		end
 	end,
	condition = function(role)
		return role.turning and role.isRunning
	end,
	onExit = function(role)
		role.turning = false
	end
}

states.jump={
	name="jump",
	relative={"fall","jumpKick","rushKick"},
	onEnter = function(role) 
		role:playAnim("jump1")
		role:playAnim("jump2",false,true)
 	end,
	condition = function(role) 
		return not role.onGround and role.dy<0 and not role.attacking 
	end

}



states.fall={
	name="fall",
	relative={"jumpKick","rushKick"},
	onEnter = function(role) 
		role:playAnim("jump3")
 	end,
	condition = function(role) 
		return not role.canJump and role.dy>=0 and not role.attacking 
	end,

	onExit = function(role) 
		role:playAnim("jump4")
 	end
}

states.leftPunch={
	name="leftPunch",
	relative={"rightPunch","doubleLeftPunch"},--"doubleLeftPunch","rightPunch","turnPunch"},
	lasting = true,
	onEnter = function(role) 
		role.canAttack = false
		role.attacking = true
		role:playAnim("hita1")
		role.currentAnim.onEvent = function(track,event)
			local name = event.data.name
			role:attackTiming(name)
		end
		role.currentAnim.onEnd =function()
			role.attacking = false
			role.canMove = true
			role.canAttack = true
			role.state:switch(nil , role.state.stack["idle"])
		end
 	end,
	condition = function(role) 
		return role.attacking and role.comboLevel == 1
	end,
}

states.rightPunch={
	name="rightPunch",
	relative={"leftHook","roundKick"},--"doubleLeftPunch","rightPunch","turnPunch"},
	lasting = true,
	onEnter = function(role) 
		role.canAttack = false
		role.attacking = true
		role:playAnim("hita2")
		role.currentAnim.onEvent = function(track,event)
			local name = event.data.name
			role:attackTiming(name)
		end
		role.currentAnim.onEnd =function()
			role.attacking = false
			role.canMove = true
			role.canAttack = true
			role.state:switch(nil , role.state.stack["idle"])
		end
 	end,
	condition = function(role) 
		return role.attacking and role.comboLevel == 2
	end,
}


states.doubleLeftPunch={
	name="doubleLeftPunch",
	relative={"rightRush"},
	lasting = true,
	onEnter = function(role) 
		role.canAttack = false
		role.attacking = true
		role:playAnim("hitb1")
		if role.facingRight then
			role.dx = 1
		else
			role.dx = -1
		end
		role.currentAnim.onEvent = function(track,event)
			local name = event.data.name
			role:attackTiming(name)
		end
		role.currentAnim.onEnd =function()
			role.attacking = false
			role.canMove = true
			role.canAttack = true
			role.state:switch(nil , role.state.stack["idle"])
		end
 	end,
	condition = function(role) 
		return role.attacking and role.comboLevel == 2 and role.attackKey == "front"
	end,
}

states.rightRush={
	name="rightRush",
	relative={},
	lasting = true,
	onEnter = function(role) 
		role.canAttack = false
		role.attacking = true
		role:playAnim("hitc1")
		if role.facingRight then
			role.dx = 3
		else
			role.dx = -3
		end
		role.currentAnim.onEvent = function(track,event)
			local name = event.data.name
			role:attackTiming(name,true)
		end
		role.currentAnim.onEnd =function()
			role.attacking = false
			role.canMove = true
			role.canAttack = true
			role.comboLevel = 0
			role.state:switch(nil , role.state.stack["idle"])
		end
 	end,
	condition = function(role) 
		return role.attacking and role.comboLevel == 3 and role.attackKey == "front"
	end,
}

states.leftHook={
	name="leftHook",
	relative={},--"doubleLeftPunch","rightPunch","turnPunch"},
	lasting = true,
	onEnter = function(role) 
		role.canAttack = false
		role.attacking = true
		role:playAnim("hitc2")
		delay:new(0.3,_,function() role.dy=-5 end)
		role.currentAnim.onEvent = function(track,event)
			local name = event.data.name
			role:attackTiming(name,true)
		end
		role.currentAnim.onEnd =function()
			role.attacking = false
			role.canMove = true
			role.canAttack = true
			role.comboLevel = 0
			role.state:switch(nil , role.state.stack["idle"])
		end
 	end,
	condition = function(role) 
		return role.attacking and role.comboLevel == 3
	end,
}

states.roundKick={
	name="roundKick",
	relative={},--"doubleLeftPunch","rightPunch","turnPunch"},
	lasting = true,
	onEnter = function(role) 
		role.canAttack = false
		role.attacking = true
		role:playAnim("hitc3")
		delay:new(0.3,_,function() role.dy=-3 end)
		role.currentAnim.onEvent = function(track,event)
			local name = event.data.name
			role:attackTiming(name,true)
		end
		role.currentAnim.onEnd =function()
			role.attacking = false
			role.canMove = true
			role.canAttack = true
			role.comboLevel = 0
			role.state:switch(nil , role.state.stack["idle"])
		end
 	end,
	condition = function(role) 
		return role.attacking and role.comboLevel == 3 and role.attackKey == "up"
	end,
}

states.jumpKick={
	name="jumpKick",
	relative={},--"doubleLeftPunch","rightPunch","turnPunch"},
	lasting = true,
	onEnter = function(role) 
		role.canAttack = false
		role.attacking = true
		role:playAnim("hitjump")
		
		if role.facingRight then
			role.dx = 5
		else
			role.dx = -5
		end

		role.currentAnim.onEvent = function(track,event)
			local name = event.data.name
			role:attackTiming(name,true)
		end
	
		role.currentAnim.onEnd =function()
			role.attacking = false
			role.canMove = true
			role.canAttack = true
			role.comboLevel = 0
			role:stopCurrentAnim()
			role.state:switch(nil , role.state.stack["walk"])
		end
 	end,
	condition = function(role) 
		return role.attacking and not role.isRunning
	end,
}


states.rushKick={
	name="rushKick",
	relative={},--"doubleLeftPunch","rightPunch","turnPunch"},
	lasting = true,
	onEnter = function(role) 
		role.canAttack = false
		role.attacking = true
		role:playAnim("hitkick")
		
		if role.facingRight then
			role.dx = 15
		else
			role.dx = -15
		end

		role.currentAnim.onEvent = function(track,event)
			local name = event.data.name
			role:attackTiming(name,true)
		end
		role.currentAnim.onEnd =function()
			role.attacking = false
			role.canMove = true
			role.canAttack = true
			role.comboLevel = 0
			role:stopCurrentAnim()
			role.state:switch(nil , role.state.stack["walk"])
		end
 	end,
	condition = function(role) 
		return role.attacking and role.isRunning
	end,
}

states.tackleKick={
	name="tackleKick",
	relative={},--"doubleLeftPunch","rightPunch","turnPunch"},
	lasting = true,
	onEnter = function(role) 
		role.canAttack = false
		role.attacking = true
		role:playAnim("hitchan")
		
		if role.facingRight then
			role.dx = 15
		else
			role.dx = -15
		end

		role.currentAnim.onEvent = function(track,event)
			local name = event.data.name
			role:attackTiming(name,true)
		end
		role.currentAnim.onEnd =function()
			role.attacking = false
			role.canMove = true
			role.canAttack = true
			role.comboLevel = 0
			role:stopCurrentAnim()
			role.state:switch(nil , role.state.stack["walk"])
		end
 	end,
	condition = function(role) 
		return role.attacking and role.isRunning and role.attackKey == "down"
	end,
}


states.rushRoll={
	name="rushRoll",
	relative={},--"doubleLeftPunch","rightPunch","turnPunch"},
	lasting = true,
	onEnter = function(role) 
		role.canAttack = false
		role.attacking = true
		role:playAnim("hitrunready")
		role:playAnim("hitgun",true,true)
		

		if role.facingRight then
			role.dx = 30
		else
			role.dx = -30
		end

		delay:new(0.3,_,function()
			role.attacking = false
			role.canMove = true
			role.canAttack = true
			role.comboLevel = 0
			role:stopCurrentAnim()
			role:playAnim("hitrunend")
			role.state:switch(nil , role.state.stack["run"])
		end)
 	end,
	condition = function(role) 
		return role.attacking and role.isRunning
	end,
}


states.behitFrontLight={
	name="behitFrontLight",
	relative={},
	lasting = true,
	onEnter = function(role)
		role.behit = true
		role:playAnim("behit1")
		role.currentAnim.onEnd = function()
			role.behit = false
		end
 	end,
	condition = function(role) 
		return role.behit
	end,
}

states.behitFrontHeavy={
	name="behitFrontHeavy",
	relative={},
	lasting = true,
	onEnter = function(role)
		role.behit = true
		role:playAnim("behit2")
		role.currentAnim.onEnd = function()
			role.behit = false
		end
 	end,
	condition = function(role) 
		return role.behit
	end,
}

states.behitBackLight={
	name="behitBackLight",
	relative={},
	lasting = true,
	onEnter = function(role)
		role.behit = true
		role:playAnim("behit4")
		role.currentAnim.onEnd = function()
			role.behit = false
		end
 	end,
	condition = function(role) 
		return role.behit
	end,
}

states.behitBackHeavy={
	name="behitBackHeavy",
	relative={},
	lasting = true,
	onEnter = function(role)
		role.behit = true
		role:playAnim("behit5")
		role.currentAnim.onEnd = function()
			role.behit = false
		end
 	end,
	condition = function(role) 
		return role.behit
	end,
}

return states