local states={}

states.idle={
	name="idle",
	relative={},
	onEnter = function(role) 
		role:playAnim("idle",true,true)
 	end,
	condition = function(role) return not role.isMoving end,
	onExit = function(role)
		role:stopCurrentAnim()
	end
}

return states